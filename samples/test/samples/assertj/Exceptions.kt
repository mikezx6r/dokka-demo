package samples.assertj

import ca.mikewilkes.testhelper.assertj.shouldThrow
import org.junit.Test
import org.junit.experimental.runners.Enclosed
import org.junit.runner.RunWith
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue

// START - shared code
typealias Sample = Test
fun assertPrints(expression: Any?, expectedOutput: String) = assertEquals(expectedOutput, expression.toString())
// END - shared code


@RunWith(Enclosed::class)
class Assertj {
    class Exceptions {
        @Sample
        fun exceptionVerification() {
            //NOTE: This test is written with assertions in an attempt to mimic what is done for Kotlin Language documentation.
            // Haven't determined if it uses a post-processor or how it converts the following assertions into

            /*
             val list = listOf(1,2,3)
             list.toString() // [1, 2, 3]
             list.size == 3  // true
             list.size == 2  // false
             shouldThrow<IllegalArgumentException> {
                // do processing
                throw IllegalArgumentException("This will cause test to pass")
            }
            */
            val list = listOf(1,2,3)
            assertPrints(list.toString(), "[1, 2, 3]")
            assertTrue(list.size == 3)
            assertFalse(list.size == 2)
            shouldThrow<IllegalArgumentException> {
                // do processing
                throw IllegalArgumentException("This will cause test to pass")
            }
        }
    }
}



# Module dokka demo

## A test library to demonstrate Dokka code samples

Provides documentation with samples from actual compiled code

# Package ca.mikewilkes.testhelper.assertj

The AssertJ library is a very powerful assertion library with many features for doing efficient comparisons of most items
and rendering meaninful messages when items don't match.

This library merely adds some syntax niceties around the assertions leveraged thus far. If a wrapper hasn't been created,
AssertJ can be leveraged directly.

For example, the following two lines are equivalent:

```kotlin
val actual = someService.process()

// Use AssertJ directly


// Use the Test Utilities wrapper
actual shouldEqual "Expected response"
```

# Package ca.mikewilkes.testhelper.jupiter

Helpers to simplify creation of Parametized test Arguments, and reduce necessity of adding @Suppress("unused") all over the code.

Forces arguments into a `description`, `input`, `expected` property pattern.

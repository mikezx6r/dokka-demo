package ca.mikewilkes.testhelper.assertj

import org.assertj.core.api.AbstractThrowableAssert
import org.assertj.core.api.Assertions.assertThatCode
import org.assertj.core.api.Assertions.assertThatThrownBy

/**
 * Ensures that the code in the Lambda throws the expected exception, and allows for additional verification
 * of the thrown exception using standard
 * [AssertJ](https://joel-costigliola.github.io/assertj/assertj-core-features-highlight.html#exception-assertion)
 * exception processing.
 *
 * @sample samples.assertj.Assertj.Exceptions.exceptionVerification
 *
 * @param T The type of the exception that should be thrown
 * @param thrower The Lambda that contains the code to execute, and that should throw an exception
 *
 * @throws AssertionError If the Lambda does not throw the expected exception
 *
 */
inline fun <reified T> shouldThrow(noinline thrower: () -> Unit): AbstractThrowableAssert<*, *> =
    assertThatThrownBy(thrower).isExactlyInstanceOf(T::class.java)

/**
 * Ensures the Lambda does not throw any exceptions.
 *
 *  @param goodCode The Lambda that contains the code to execute, and that should not throw an exception
 *
 *  @throws AssertionError if the Lambda throws an exception
 */
fun doesNotException(goodCode: () -> Unit) =
    assertThatCode(goodCode).doesNotThrowAnyException()

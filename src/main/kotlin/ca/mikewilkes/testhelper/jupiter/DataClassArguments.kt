package ca.mikewilkes.testhelper.jupiter

/**
 * For creating a Parameterized test argument that has a description and a single input value.
 *
 * Note the input value could be an object
 *
 * @param description describes the test case in plain language
 * @param value the input value or object for the test
 */
data class SingleDescribedArguments<T : Any?>(val description: String, val value: T)

/**
 * For creating a Parameterized test argument that has a description, input values, and an expected value.
 *
 * Note the input and the expected can be objects.

 * @param description describes the test case in plain language
 * @param value the input value or object for the test
 * @param expected The expected value or object container for the test
 */
data class SingleExpectedDescribedArguments<T : Any?, R : Any?>(val description: String, val value: T, val expected: R)

/**
 * For creating a Parameterized test argument that has an input values and an expected value.
 *
 * Note the input and the expected can be objects.
 *
 * @param value the input value or object for the test
 * @param expected The expected value or object container for the test
 */
data class SingleExpectedArguments<T : Any?, R : Any?>(val value: T, val expected: R)
